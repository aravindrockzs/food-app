
import AddButton from './AddButton';
import styles from './ItemCard.module.css'
function ItemCard({ title, tag, price, img }) {
  return (
    <div className={styles.itemCardContainer}>
      <div className={styles.itemImgContainer}>
        <img className={styles.itemImg} src={img} alt=" food item" />
      </div>
      <div className={styles.itemTagContainer}>
        <div className={styles.itemTag}>
          {tag}
        </div>
      </div>
      <div className={styles.itemTitle}>
        {title}
      </div>
      <div className={styles.itemPriceAdd}>
        <div className={styles.itemPrice}>
          {price}
        </div>
        <AddButton />
      </div>
    </div>
  );
}

export default ItemCard;