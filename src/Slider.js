import styles from './Slider.module.css'
import pizza from './assets/pizza.png'
import bowl1 from './assets/bowl_1.png'
import { useState, useEffect } from 'react';

import React from 'react';

const offers = [
  { 
    pic: bowl1,
    color: "purple",
    description: "get upto 50% off for this particular item when you purchase for more than 200Rs"
  },
  {
    pic: pizza,
    color: "yellow",
    description: "get upto 70% off for this particular item when you purchase for more than 200Rs"
  },
  {
    pic: pizza,
    color: "purple",
    description: "get upto 50% off for this particular item when you purchase for more than 200Rs"
  }
]

function Offers() {
  const [index, setIndex] = useState(0);


  useEffect(() => {
    const id=setInterval(() => {
      setIndex((prevIndex)=>prevIndex === offers.length - 1 ? 0 : prevIndex + 1)
    }, 4000)
    return () => { clearInterval(id)};

  }, [index]);
  return (
    <div className={styles.container}>
      <div className={styles.slider} style={{ transform: `translate(${-index * 100}%)` }}>
        {
          offers.map((item, index) => {
            return (
              <div className={styles.itemContainer} style={{ backgroundColor: item.color }}>
                  <div className={styles.itemDesc}>
                    {item.description}
                  </div>
                  <div className={styles.itemImgContainer}>
                    <img className={styles.itemImg} src={item.pic} />
                  </div>
              </div>

            )
          })
        }
       
      </div>
      <div  className={styles.sliderBtnsContainer}>
        {
          offers.map((_,idx)=>(<div onClick={()=>setIndex(idx)} className={index===idx? styles.sliderbuttonsactive:styles.sliderbuttons } /> ))
        }

      </div>
      
    </div>
  )


}

export default Offers;