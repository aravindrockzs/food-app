import styles from './MoodCard.module.css'

function MoodCard({bgstyle,data}) {
    return ( 
        <div className={styles.moodContainer} style={bgstyle}> 
          <img className={styles.moodImg} src={data}/>
          <div>Mood Booster</div>
        </div>
     );
}

export default MoodCard;