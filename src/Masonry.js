import styles from './Masonry.module.css'


function Masonry(props) {
    return ( 
        <div className={styles.masonryContainer}>
            {props.children}
        </div>
     );
}

export default Masonry;