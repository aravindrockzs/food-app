import styles from './SearchBox.module.css'
import search from './assets/search.svg'

function SearchBox({filter}) {
    return ( 
    <div className={styles.searchBox}>
        <div>
          <img className={styles.searchIcon} src={search} alt="Search" />
        </div>
        <input onChange={(e)=>filter(e.target.value)} className={styles.searchInput} placeholder="Search" type="text" />
    </div> 
    );
}

export default SearchBox;