import plusicon from './assets/plus1.png'

import styles from './AddButton.module.css'


function AddButton() {
    return ( 
        <div className={styles.addbtnContainer}>
            <img className={styles.addbtnImg} src={plusicon} alt="plus icon"  />
        </div>
     );
}

export default AddButton;