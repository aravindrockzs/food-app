import styles from './Homepage.module.css';

import Slider from './Slider.js'
import Options from './Options';
import SearchBox from './SearchBox';
import Moods from './Moods'

import { useState } from 'react'
import Recommended from './Recommended';







function Homepage() {
  const [searchfilter, setsearchfilter] = useState('');
  const [mood, setmood] = useState('');
    const [active, setactive] = useState('mood');  
  return (
    <div className={styles.container}>
      <SearchBox filter={setsearchfilter}/>
      <Slider/>
      <Options>
        <div className={styles.optionsContainer}>
          <div onClick={()=>setactive('mood')} className={active==='mood'? styles.active : styles.notActive}>Moods</div>
          <div onClick={()=>setactive('menu')}  className={active==='menu'? styles.active : styles.notActive }> Traditional Menu</div>
        </div>
        <Moods setmood={setmood}/>

      </Options>
      <Recommended search={searchfilter}/>

      





     
    </div>
  );
}

export default Homepage;