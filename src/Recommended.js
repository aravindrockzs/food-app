import bowl1 from './assets/bowl_1.png'
import ItemCard from './ItemCard';
import Masonry from './Masonry';
import styles from './Recommended.module.css'
const items = [
  {
    img: bowl1,
    title: 'Acai berry bowl',
    tag: 'low histamine',
    price: '$10'
  },
  {
    img: bowl1,
    title: 'Briyani',
    tag: 'low histamine',
    price: '$10'
  }
]


function Recommended({ search }) {
  return (
    <div className={styles.recommendContainer}>
      <div className={styles.recommendTitles}>
        <div>Recommended</div>
      </div>
      <div className={styles.itemCards}>
        <Masonry>
          {
            search ? items.filter(({ title }) => title.includes(search))
              .map(({ title, tag, price, img }) => <ItemCard title={title} tag={tag} price={price} img={img} />)
              : items.map(({ title, tag, price, img }, idx) => <ItemCard title={title} tag={tag} price={price} img={img} />)
          }
        </Masonry>
      </div>
    </div>
  );
}

export default Recommended;