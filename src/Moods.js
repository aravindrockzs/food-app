import sun1 from './assets/sun1.png'
import brain from './assets/brain.png'
import happy from './assets/happy.png'
import friends from './assets/friends.png'



import styles from './Moods.module.css'
import MoodCard from './MoodCard'


function Moods({setmood}) {
    return (
      <div className={styles.container}>
        <MoodCard data={sun1} bgstyle={{backgroundColor: '#FEEAB1'}}/>
        <MoodCard data={brain} bgstyle={{backgroundColor: '#FAD4DE'}}/>
        <MoodCard data={friends} bgstyle={{backgroundColor: '#D7EDFC'}}/>
        <MoodCard data={happy} bgstyle={{backgroundColor: '#FBDFCF'}}/>
        <MoodCard data={sun1} bgstyle={{backgroundColor: '#FEEAB1'}}/>
      </div>

    );
}

export default Moods;